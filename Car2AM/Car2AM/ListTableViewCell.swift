//
//  ListTableViewCell.swift
//  Car2AM
//
//  Created by Suren on 03.09.18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var listCellImage: UIImageView!
    @IBOutlet weak var listCellTitle: UILabel!
    @IBOutlet weak var listCellAddress: UILabel!
    @IBOutlet weak var listCellRating: UIStackView!
    @IBOutlet weak var listCellStationType: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func listCellMapAction(_ sender: UIButton) {
        
    }
    
    @IBAction func listCellShareAction(_ sender: UIButton) {
        
    }
    
    
}

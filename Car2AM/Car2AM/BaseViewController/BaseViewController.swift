//
//  BaseViewController.swift
//  Car2AM
//
//  Created by Suren on 10/3/18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // for all navigation controller's Back button
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

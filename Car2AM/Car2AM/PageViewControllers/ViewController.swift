//
//  ViewController.swift
//  Car2AM
//
//  Created by Suren on 14.08.18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

// http://car2.am/movingservice/GetTestObject

import UIKit

class ViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var buttonNext: UIButton!
    
    var pageViewController: UIPageViewController!
    let pageTitles = ["Գտեք այն ամենն ինչ անհրաժեշտ է Ձեր մեքենային:", "Խնայեք Ձեր ժամանակը, գտեք համապատասխան ծառայությունը կամ խանութը, այցելեք ու քնահատեք այն:", "Օգտվեք առկա ծառայություններից ցանկացած վայրից:  Միացեք!"]
    let pageImages = ["tutorial_page_1.jpg", "tutorial_page_2.jpg", "tutorial_page_3.jpg"]
    
    var pageContentViewController: ContentPageViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        buttonNext.layer.cornerRadius = 5
        
        pageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageViewController") as? UIPageViewController
        
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self
        
        let startingViewController = viewControllerAtIndex(index: 0)
        pageViewController.setViewControllers([startingViewController!], direction: .forward, animated: true, completion: nil)

        pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 150)
        self.addChildViewController(pageViewController)
        self.view.addSubview(pageViewController.view)
        self.pageViewController.didMove(toParentViewController: self)

    }

    func viewControllerAtIndex(index: Int) -> ContentPageViewController? {
        
        if pageTitles.count <= 0 || index >= pageTitles.count {
            return nil
        }
        
        pageContentViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContentPageViewController") as? ContentPageViewController
        pageContentViewController.imageFile = pageImages[index]
        pageContentViewController.titleText = pageTitles[index]
        pageContentViewController.pageIndex = index
  
        return pageContentViewController
        
    }

    
    // From PageViewController to StartViewController
    func moveToStartViewController() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "StartViewController")
        self.present(vc, animated: true, completion: nil)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let currentViewController = viewController as! ContentPageViewController

        if currentViewController.pageIndex == 0 {
            return nil
        }

        return self.viewControllerAtIndex(index: currentViewController.pageIndex - 1)

    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let currentViewController = viewController as! ContentPageViewController
        
        if currentViewController.pageIndex == pageTitles.count - 1 {
            return nil
        }

        return self.viewControllerAtIndex(index: currentViewController.pageIndex + 1)
    }


    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pageTitles.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    

    //UIPageViewControllerDelegate pageControl dots
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let current = pageViewController.viewControllers?.first as! ContentPageViewController

        if completed {
           pageControl.currentPage = current.pageIndex
        }
    }

}


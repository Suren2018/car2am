//
//  ContentPageViewController.swift
//  Car2AM
//
//  Created by Suren on 23.08.18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

import UIKit

class ContentPageViewController: UIViewController {
    
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var backgroundText: UILabel!
    
    var pageIndex: Int!
    var titleText: String!
    var imageFile: String!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.backgroundImage.image = UIImage(named: imageFile)
        self.backgroundText.text = titleText
    }


}

//
//  CarModelTableViewController.swift
//  Car2AM
//
//  Created by Suren on 10/13/18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

import UIKit

protocol PassButtonTitle {
    func passTitle(title: String, checkFlag: Bool)
}

class CarModelTableViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var carModelList = [String]()
    var kindCarTitles = [String]()
    var data = [String]()
    var flag: Bool!
    var delegate: PassButtonTitle!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
      
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        cell?.textLabel?.text = data[indexPath.row]

        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if flag {
            
            delegate.passTitle(title: data[indexPath.row], checkFlag: flag)
            
            
        } else {
            delegate.passTitle(title: data[indexPath.row], checkFlag: flag)
    
            
        }


        self.navigationController?.popViewController(animated: true)

        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    */

}

//
//  EntranceViewController.swift
//  Car2AM
//
//  Created by Suren on 10/3/18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

import UIKit

class EntranceViewController: BaseViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var enterPhoneNumberTextField: UITextField!
    @IBOutlet weak var enterPasswordTextfield: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    
    var placeHolderPhoneNumber = NSMutableAttributedString()
    var placeholderPassword = NSMutableAttributedString()
    var placeholderColor = NSMutableAttributedString()
    let phoneNumber = "Մուտքագրեք հեռախոսահամարը"
    let password = "Մուտքագրեք գաղտնաբառը"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.enterButton.layer.cornerRadius = 5
        
        self.enterPhoneNumberTextField.delegate = self
        self.enterPasswordTextfield.delegate = self
        self.enterPhoneNumberTextField.borderStyle = UITextBorderStyle.roundedRect
        self.enterPasswordTextfield.borderStyle = UITextBorderStyle.roundedRect

        placeholderSetting(placeholderText: phoneNumber, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), textField: enterPhoneNumberTextField)
        placeholderSetting(placeholderText: password, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), textField: enterPasswordTextfield)
        
    }
    

    @IBAction func enter(_ sender: UIButton) {
        
        if (self.enterPhoneNumberTextField.text?.isEmpty)! {
            placeholderSetting(placeholderText: phoneNumber, color: UIColor.red, textField: enterPhoneNumberTextField)
        }
        
        if (self.enterPasswordTextfield.text?.isEmpty)! {
            placeholderSetting(placeholderText: password, color: UIColor.red, textField: enterPasswordTextfield)
        }
    }
    
    // placeholders fontSize and color
    func placeholderSetting(placeholderText: String, color: UIColor, textField: UITextField) {
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(string: placeholderText, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica", size: 12.0)!])
        placeholder.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: NSRange(location: 0, length: placeholderText.count))
        textField.attributedPlaceholder = placeholder
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

    */

}

//
//  RegistrationViewController.swift
//  Car2AM
//
//  Created by Suren on 10/3/18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

import UIKit

class RegistrationViewController: BaseViewController, UITextFieldDelegate, PassButtonTitle {

    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var carModel: UIButton!
    @IBOutlet weak var kind: UIButton!
    @IBOutlet weak var sex: UIButton!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var femail: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    @IBOutlet weak var email: UITextField!

    
    let namePlaceholder = "Մուտքագրեք Ձեր անունը*"
    let femailPlaceholder = "Մուտքագրեք Ձեր ազգանունը*"
    let passwordPlaceholder = "Մուտքագրեք գաղտնաբառը*"
    let repeatPasswordPlaceholder = "Կրկնեք գաղտնաբառը*"
    let emailPlacceholder = "Մուտքագրեք Ձեր էլ.հասցեն"
    let sexPlaceholder = "Սեռ*"
    var carModelPlaceholder = "Մակնիշ"
    var kindPlaceholder = "Տեսակ"
    let notEqualPassword = "Գաղտնաբառը չի համընկնում"
    let male = "Արական"
    let female = "Իգական"
    let chooseSex = "Ընտրել սեռը"
    let validEmail = "Մուտքագրել վավեր էլ. հասցե"
    let passwordLength = "Գաղտնաբառի երկարությունը պետք է լինի > 5"

    var registrationIndex: Int!
    var tempStringForButton = ""
    var flag = true
    var jsonData: [CarList]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.kind.isEnabled = false
        
        self.name.delegate = self
        self.femail.delegate = self
        self.password.delegate = self
        self.repeatPassword.delegate = self
        self.email.delegate = self
        
        self.registerButton.layer.cornerRadius = 5
        self.carModel.layer.cornerRadius = 5
        self.carModel.layer.borderWidth = 0.5
        self.carModel.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1).cgColor
        
        // Buttons 7 point from left side button
        self.carModel.titleEdgeInsets.left = 7
        self.kind.titleEdgeInsets.left = 7
        self.sex.titleEdgeInsets.left = 7
        
        self.kind.layer.cornerRadius = 5
        self.kind.layer.borderWidth = 0.5
        self.kind.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1).cgColor
        
        self.sex.layer.cornerRadius = 5
        self.sex.layer.borderWidth = 0.5
        self.sex.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1).cgColor
        
        fontSizeForButtonTitles(title: carModelPlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), button: carModel, fontSize: 12)
        fontSizeForButtonTitles(title: kindPlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), button: kind, fontSize: 12)
        fontSizeForButtonTitles(title: sexPlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), button: sex, fontSize: 12)
        
        placeholderSetting(placeholderText: namePlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), textField: name)
        placeholderSetting(placeholderText: femailPlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), textField: femail)
        placeholderSetting(placeholderText: passwordPlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), textField: password)
        placeholderSetting(placeholderText: repeatPasswordPlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), textField: repeatPassword)
        placeholderSetting(placeholderText: emailPlacceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), textField: email)
        
        self.name.borderStyle = UITextBorderStyle.roundedRect
        self.femail.borderStyle = UITextBorderStyle.roundedRect
        self.password.borderStyle = UITextBorderStyle.roundedRect
        self.repeatPassword.borderStyle = UITextBorderStyle.roundedRect
        self.email.borderStyle = UITextBorderStyle.roundedRect
        
        getJSONData()
    }
    
    
    @IBAction func registrationButton(_ sender: UIButton) {
        
        if (self.name.text?.isEmpty)! {
            placeholderSetting(placeholderText: namePlaceholder, color: .red, textField: name)
        }
        if (self.femail.text?.isEmpty)! {
            placeholderSetting(placeholderText: femailPlaceholder, color: .red, textField: femail)
        }
        if (self.password.text?.isEmpty)! {
            placeholderSetting(placeholderText: passwordPlaceholder, color: .red, textField: password)
        }
        // Check password length, which will be > 5
        if (self.password.text?.count)! < 6 {
            let alert = UIAlertController(title: nil, message: self.passwordLength, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: {(action) in
                self.password.text = ""
                self.repeatPassword.text = ""
            })
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
        if (self.repeatPassword.text?.isEmpty)! {
            placeholderSetting(placeholderText: repeatPasswordPlaceholder, color: .red, textField: repeatPassword)
        }
        if self.password.text != self.repeatPassword.text {
            self.password.text = ""
            self.repeatPassword.text = ""
            placeholderSetting(placeholderText: notEqualPassword, color: .red, textField: password)
            placeholderSetting(placeholderText: notEqualPassword, color: .red, textField: repeatPassword)
        }
        
        if !(self.email.text?.isValidEmail())! {
            let alert = UIAlertController(title: "", message: self.validEmail, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: {(action) in
                return
            })
            
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    // Change Button title Արական or Իգական
    @IBAction func sexButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: self.chooseSex, preferredStyle: .actionSheet)
        let male = UIAlertAction(title: self.male, style: .default, handler: {(action) in
            self.fontSizeForButtonTitles(title: self.male, color: .black, button: self.sex, fontSize: 16)
        })
        let female = UIAlertAction(title: self.female, style: .default, handler: {(action) in
            self.fontSizeForButtonTitles(title: self.female, color: .black, button: self.sex, fontSize: 16)
        })
        
        alert.addAction(male)
        alert.addAction(female)
        present(alert, animated: true, completion: nil)
        
    }
    
    
    // placeholders fontSize and color
    func placeholderSetting(placeholderText: String, color: UIColor, textField: UITextField) {
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(string: placeholderText, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica", size: 12.0)!])
        placeholder.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: NSRange(location: 0, length: placeholderText.count))
        textField.attributedPlaceholder = placeholder
    }
    
    
    // Button title size and color
    func fontSizeForButtonTitles(title: String, color: UIColor, button: UIButton, fontSize: Int) {
        let attribute = NSAttributedString(string: title, attributes: [NSAttributedStringKey.foregroundColor : color,
                                                                       NSAttributedStringKey.font : UIFont(name: "Helvetica", size: CGFloat(fontSize))!])
        button.setAttributedTitle(attribute, for: .normal)
    }

    
    // hide keyborad
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    // hide keyborad
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    // hide keyborad
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let carModelVC = segue.destination as! CarModelTableViewController
        var car_list = [String]()
        
        if segue.identifier == "carModel" {
            carModelVC.flag = self.flag
            
            for i in jsonData {
                car_list.append(i.title)
            }
        }
        if segue.identifier == "carKind" {
            carModelVC.flag = !self.flag
            for item in jsonData {
                if item.title == self.tempStringForButton {
                    for j in item.models {
                        car_list.append(j.title)
                        
                    }
                }
                
            }
        }
        carModelVC.data = car_list
        carModelVC.delegate = self
     }

     
    // MARK: Delegate method
    func passTitle(title: String, checkFlag: Bool) {
        
        self.kind.isEnabled = true
        if checkFlag {
            fontSizeForButtonTitles(title: kindPlaceholder, color: UIColor(red: 150/255, green: 150/255, blue: 160/255, alpha: 1), button: kind, fontSize: 12)
            self.tempStringForButton = title
            
            self.carModel.setTitle(title, for: .normal)
            fontSizeForButtonTitles(title: title, color: .black, button: carModel, fontSize: 16)
            
        } else {
            
            self.kind.setTitle(title, for: .normal)
            fontSizeForButtonTitles(title: title, color: .black, button: kind, fontSize: 16)
        }
    }

 
    
    // Get JSON data from local file car_list.json
    func getJSONData() {
        guard let path = Bundle.main.path(forResource: "car_list", ofType: "json") else {
            print("NO DATA")
            return
        }
        
        do {
            let url = try Data(contentsOf: URL(fileURLWithPath: path))
            jsonData = try JSONDecoder().decode([CarList].self, from: url)

        } catch {
            print(error.localizedDescription)
        }
    }
    

}


struct CarList: Decodable {
    let value: String
    let title: String
    let models: [CarModels]
}
struct CarModels: Decodable {
    let value: String
    let title: String
}

// Email address validation
extension String {
    func isValidEmail() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}

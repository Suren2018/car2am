//
//  StartViewController.swift
//  Car2AM
//
//  Created by Suren on 24.08.18.
//  Copyright © 2018 Car2AM. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    @IBOutlet weak var buttonForFacebook: UIButton!
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var buttonEntrance: UIButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonForFacebook.layer.cornerRadius = 5
        buttonRegister.layer.cornerRadius = 5
        buttonEntrance.layer.cornerRadius = 5
        
    }
    
    @IBAction func facebookAction(_ sender: UIButton) {
        
        let Username =  "instagram" // Your Instagram Username here
        let appURL = URL(string: "instagram://user?username=\(Username)")!
        let application = UIApplication.shared
        
        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            // if Instagram app is not installed, open URL inside Safari
            let webURL = URL(string: "https://instagram.com/\(Username)")!
            application.open(webURL)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        self.buttonForFacebook.center.y += 150
        self.buttonForFacebook.alpha = 0
        self.buttonRegister.center.y += 100
        self.buttonRegister.alpha = 0
        self.buttonEntrance.center.y += 50
        self.buttonEntrance.alpha = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1.0, delay: 0.1, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.4, options: [], animations: {
            self.buttonForFacebook.center.y -= 150
            self.buttonForFacebook.alpha = 1
            self.buttonRegister.center.y -= 100
            self.buttonRegister.alpha = 1
            self.buttonEntrance.center.y -= 50
            self.buttonEntrance.alpha = 1
        }, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // for json example
    func jsonExample() {
                let url = "https://car2.am/movingservice/GetTestObject"
 //       let url = "https://restcountries.eu/rest/v2/all"
        let urlObject = URL(string: url)
        
        URLSession.shared.dataTask(with: urlObject!, completionHandler: {(data, response, error) in
            
            guard let data = data else {
                print("No Data")
                return
            }
            
            do {
                let object = try JSONDecoder().decode([GetRequest].self, from: data)
                
                
                for i in object {
                    print(i.massege + " : " + i.user)
                }
                
            } catch {
                print("error")
            }
        }).resume()
        
    }

}

struct GetRequest: Decodable {
    
    let massege: String
    let user: String
//    let name: String
//    let region: String
}
